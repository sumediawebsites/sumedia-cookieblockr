// describe('Sumedia', () => {
//     beforeAll(async () => {
//         await page.setViewport({
//             width: 1920,
//             height: 1080
//         });
//         await page.goto('http://bearnibbles.local');
//         await page.screenshot({path: 'SCREENSHOT.png'});

//         await page.setViewport({
//             width: 375,
//             height: 667
//         });
//         await page.goto('http://bearnibbles.local');
//         await page.screenshot({path: 'SCREENSHOT_MOBILE.png'});
//     });

//     it('should be titled "Homepage | Bear Nibbles"', async () => {
//         await expect(page.title()).resolves.toMatch('Homepage | Bear Nibbles');
//     });
// });

const puppeteer = require('puppeteer');
const { toMatchImageSnapshot } = require('jest-image-snapshot');
 
expect.extend({ toMatchImageSnapshot });

const browserConfig = {
    ignoreHTTPSErrors: true,
    headless: true,
    args: ['--no-sandbox', '--disable-setuid-sandbox'],
};
   
// jest-image-snapshot custom configuration in order to save screenshots and compare the with the baseline
function setConfig (filename) {
    return {
        failureThreshold: '0.01',
        failureThresholdType: 'percent',
        customSnapshotsDir: `${__dirname}/snapshots/`,
        customSnapshotIdentifier: filename,
        noColors: true
    }
}
   
  describe('screenshot', () => {
    let browser;
   
    beforeAll(async () => {
      // start Puppeteer with a custom configuration, see above the setup
      browser = await puppeteer.launch(browserConfig);
    });
   
    it('dummy application page', async () => {
      const page = await browser.newPage();
   
      expect.extend({ toMatchImageSnapshot });
      await page.goto('https://bearnibbles.co.uk');
    //   await page.goto('http://bearnibbles.local');
      await page.setViewport({ width: 1920, height: 1080 });
    
      const image = await page.screenshot({ fullPage: true });
      expect(image).toMatchImageSnapshot(setConfig('image-name'));
    }, 15000);
   
    afterAll(async () => {
      await browser.close();
    });
});