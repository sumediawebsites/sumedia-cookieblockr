import isEmpty from './util/isEmpty';

/**
 * @param {Object} [options] - Object of config settings
 * @param {string} [options.selector] - Selector of cookiebar element
 * @param {string} [options.cookiename] - Name of cookie that stores array of accepted cookietypes
 * @param {string} [options.hiddenClass] - Class to give cookiebar when it should be hidden.
 * @param {string} [options.btnSelector] - Selector of CTA button to submit cookiechoices.
 * @param {string} [options.checkboxSelector] - Selector of checkboxes
 * @param {string} [options.formSelector] - Selector of form in cookiebar
 * @param {string} [options.selectedInputSelector=input:checked] - Selector of checked checkbox
 */
const initCookieBar = options => {
    // Check if settings were passed. If not, throw error.  
    if (isEmpty(options)) {
        throw new Error('No options were passed.')
    }

    const cookiebar = document.querySelector(options.selector);
    
    // Stop this function from executing if no cookiebar is found in the DOM
    if ( ! cookiebar && getCookie(options.cookiename).length > 0 ) {
        loadAcceptedScripts()
        return
    }

    // Only add class if cookie has not been accepted yet
    if ( ! getCookie(options.cookiename).length > 0 ) {
        cookiebar.classList.add(options.hiddenClass);
    }
    
    // Let's create a function that we can bind to our eventListener
    const acceptCookies = () => {
        cookiebar.classList.remove(options.hiddenClass);

        // Let's remove the eventListener since we don't need it anymore.
        document.querySelector(options.btnSelector).removeEventListener( 'click', acceptCookies );
        
        // Get a nodelist of all the selected checkboxes
        const checkedBoxes = document.querySelectorAll(`${ options.formSelector } ${ options.selectedInputSelector ? options.selectedInputSelector : 'input:checked' }`);

        // Building an array of all accepted cookie types
        let acceptedCookieTypes = new Array();

        for (let i = 0; i < checkedBoxes.length; i += 1) {
            const checkbox = checkedBoxes[i];
            acceptedCookieTypes.push(checkbox.value);
        }
        
        // Setting the cookie with a stringified version of the array we just built 
        setCookie(options.cookiename, JSON.stringify(acceptedCookieTypes), 30);

        // Now that the cookie is set, let's start loading the scripts
        loadAcceptedScripts()

        // Wait untill the animation had a chance to finish
        setTimeout( () => {
            // Let's remove the cookiebar from the DOM, since we don't need it anymore
            cookiebar.parentNode.removeChild(cookiebar);
        }, 300);
    };

    // Initiate the eventListener so the user can accept cookies.
    document.querySelector(options.btnSelector).addEventListener( 'click', acceptCookies );

    const checks = document.querySelectorAll(options.checkboxSelector);

    for (let el = 0; el < checks.length; el += 1) {
        const element = checks[el];
        
        element.addEventListener( 'click', function () {
            this.previousElementSibling.previousElementSibling.click();
        });
    }

    // Make sure form is not submittable
    document.getElementById(options.formSelector).addEventListener( 'submit', (e) => {
        e.preventDefault();
    });
};

const setCookie = (cname, cvalue, exdays) => {
    const d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    const expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires;
};

const getCookie = cname => {
    const name = cname + "=",
    decodedCookie = decodeURIComponent(document.cookie),
    ca = decodedCookie.split(';');

    for (let i = 0; i < ca.length; i += 1) {
        const c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }

    return "";
}

const loadAcceptedScripts = () => {    
    const cookieTypesToLoad = JSON.parse(getCookie(options.cookiename));
    
    if ( ! cookieTypesToLoad.length )
        return
    
    for (let ct = 0; ct < cookieTypesToLoad.length; ct += 1) {
        const cookieType = cookieTypesToLoad[ct];
        const scripts = document.querySelectorAll(`[data-cookietype="${ cookieType }"]`);        
        
        for (let s = 0; s < scripts.length; s += 1) {
            const script = scripts[s];
            window.yett.unblock( script.src );
        }
    }
}

export { initCookieBar, setCookie }